import React, {useState, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import useGeolocation from '../Components/Geolocation/useGeolocation';

const Information = () => {
  const [location, setLocation] = useState(null);
  const [click, setClick] = useState(0);

  useEffect(() => {
    setLocation(useGeolocation());
  }, [click]);

  const handleClick = () => {
    useGeolocation();
    setClick(click + 1);
  };
  return (
    <View>
      <Text>{JSON.stringify(location)}</Text>
      <Button title="click" onPress={handleClick}></Button>
    </View>
  );
};

export default Information;
