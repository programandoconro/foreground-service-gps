import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import useGeolocation from '../Geolocation/useGeolocation';
import {postLocation} from '../../Utils/requests';

const handleServicePost = async () => {
  //await postLocation(useGeolocation());
  console.log(useGeolocation())
};

ReactNativeForegroundService.add_task(() => handleServicePost(), {
  delay: 3000,
  onLoop: true,
  taskId: 'taskid',
  onError: e => console.log(`Error logging:`, e),
});

export default ReactNativeForegroundService;
