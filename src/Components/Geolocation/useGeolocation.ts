import {useState} from 'react';
import RNLocation from 'react-native-location';

var userLocation: any;
RNLocation.configure({
  distanceFilter: 100, // Meters
  desiredAccuracy: {
    ios: 'best',
    android: 'balancedPowerAccuracy',
  },
  // Android only
  androidProvider: 'auto',
  interval: 5000, // Milliseconds
  fastestInterval: 10000, // Milliseconds
  maxWaitTime: 5000, // Milliseconds
  // iOS Only
  activityType: 'other',
  allowsBackgroundLocationUpdates: false,
  headingFilter: 1, // Degrees
  headingOrientation: 'portrait',
  pausesLocationUpdatesAutomatically: false,
  showsBackgroundLocationIndicator: false,
});
var permission = false;
RNLocation.requestPermission({
  ios: 'whenInUse',
  android: {
    detail: 'coarse',
  },
}).then(granted => {
  permission = true;
});

const useGeolocation = () => {
  console.log(permission);
  if (permission) {
    RNLocation.subscribeToLocationUpdates(locations => {
      userLocation = locations;
      console.log(locations);
    });
    RNLocation.getLatestLocation({timeout: 60000}).then(latestLocation => {
      // Use the location here
      console.log(latestLocation);
    });
  }
  return userLocation;
};

export default useGeolocation;
